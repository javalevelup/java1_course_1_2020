package org.levelup.lesson9;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class StringReplace {

    public static void main(String[] args) {
        String original = "Hey, I, love Java!";
        String newString = original.replace(",", "")
                .replace("!", "");

        System.out.println(newString);
        System.out.println(original.replaceAll("([,!])", ""));
    }

}
