package org.levelup.lesson9.lambda;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Lambdas {

    public static void main(String[] args) {
        // transform integer to hex string
        Function<Integer, String> functionAsClass = new Function<Integer, String>() {
            @Override
            public String apply(Integer integer) {
                return "0x" + Integer.toHexString(integer).toUpperCase();
            }
        };

        System.out.println(functionAsClass.apply(878679));
        // Interface:
        //  должен иметь только один метод, который нужно переопределить
        //  может быть помечененным аннотацией @FunctionalInterface (необязательное условие)

        // R apply(T t); -> String apply(Integer value)

        // Lambda
        //  1. Аргументы метода, который мы переопределяем:
        //      0 аргументов: ()
        //      1 аргумент: (val) / val
        //      2 и более аргументов: (val1, val2, val3)
        //  2. Ставится стрелочка: ->
        //  3. Тело метода, который мы переопределяем
        //       Если тело состоит из одной операции (одного выражения), то тогда просто пишем выражаение: object != null, точка с запятой не нужна, return не нужен
        //       Иначе, ставятся фигурные скобки и нужен return (если тип возвращаемого значения не void)
        //          { sout(""); return object != null;  }

        Function<Integer, String> functionAsLambda = integer -> "0x" + Integer.toHexString(integer).toUpperCase();
        System.out.println(functionAsLambda.apply(87786));

        Collection<Integer> amounts = new ArrayList<>();
        amounts.add(85734);
        amounts.add(435134);
        amounts.add(45456);
        amounts.add(38455);
        amounts.add(85729);
        amounts.add(95848);
        amounts.add(85734);

        // for (Integer amount : amounts) { sout(amount); }
//        amounts.forEach(amount -> System.out.println(amount));
         amounts.forEach(System.out::println);

        List<String> amountsAsString = amounts.stream() // Stream<Integer>
                .map(amount -> Integer.toString(amount)) // Stream<String>
                .filter(amount -> amount.length() > 5)  // Stream<String>
                .collect(Collectors.toList());
        System.out.println();
        amountsAsString.forEach(el -> System.out.println(el));
    }

}
