package org.levelup.lesson9;


import org.levelup.lesson5.Point;

@SuppressWarnings("ALL")
public class StringExample {

    public static void main(String[] args) {
        // String - immutable, любая строка immutable
        String str = "I love"; // new String("I love");
        str = str + " Java"; // "I love Java"

        // str.substring(2)
        str = str.replace("Java", "C++");
        String changedString = str.replace("Java", "C++");

        String s = str.replace(",", "");

        System.out.println(changedString.hashCode());
        // System.out.println(s == str);
        System.out.println(str.hashCode());
        System.out.println(str);

        int val = 10;
        Point point = new Point(10, 0);

        // All variables pass to methods by value
        // Все переменные передаются в метод как значение
        val = increaseValue(val);
        increaseXInPoint(point);

        System.out.println(val);
        System.out.println(point.x);

        // point.x = 20;
        increasePoint(point);
        System.out.println(point.x);

        String string = "my string";
        changeString(string);
        System.out.println(string);

        System.out.println();
        System.out.println("String comparison");
        String s1 = "Java";
        String s2 = "Java";
        String s3 = new String("Java");
        System.out.println(s1 == s2);
        System.out.println(s1 == s3);

    }

    static int increaseValue(int value) {
        value = value + 10;
        return value;
    }

    static void increaseXInPoint(Point point) {
        point.x = point.x + 10;
    }

    static void increasePoint(Point point) {
        point = new Point(point.x + 10, 0);
    }

    static void changeString(String str) {
        str = str + " my best string";
    }

}
