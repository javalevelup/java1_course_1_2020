package org.levelup.lesson9;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class RemoveDuplicates {

    public static void main(String[] args) {
        Collection<Integer> integers = new ArrayList<>();
        integers.add(5);
        integers.add(6);
        integers.add(7);
        integers.add(8);
        integers.add(5);

        Collection<Integer> newIntegers = new ArrayList<>();
        for (Integer el : integers) {
            if (!newIntegers.contains(el)) {
                newIntegers.add(el);
            }
        }

        Collection<Integer> withoutDuplicates = new HashSet<>(integers);
        System.out.println(withoutDuplicates);
    }

}
