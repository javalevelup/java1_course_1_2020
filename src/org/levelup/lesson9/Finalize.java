package org.levelup.lesson9;

public class Finalize {

    static Finalize instance = null;

    // DO NOT DO IT!
    @Override
    protected void finalize() throws Throwable {
        instance = this;
    }

}
