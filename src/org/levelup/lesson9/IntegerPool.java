package org.levelup.lesson9;

@SuppressWarnings("ALL")
public class IntegerPool {

    public static void main(String[] args) {
        // boxing/unboxing
        // autoboxign/autounboxing

        // IntegerPool - [-128, 127]

        Integer i1 = 126; // -> Integer.valueOf(126);
        Integer i2 = 126;
        Integer i3 = 129;
        Integer i4 = 129;

        System.out.println("i1 == i2: " + (i1 == i2));
        System.out.println("i3 == i4: " + (i3 == i4));
    }

}
