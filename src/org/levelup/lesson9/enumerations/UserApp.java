package org.levelup.lesson9.enumerations;

@SuppressWarnings("ALL")
public class UserApp {

    public static void main(String[] args) {
        UserManager userManager = new UserManager();
        userManager.getUserLogins("user"); // getUserLogins("abrakadabra");

        UserRole role = UserRole.USER;
        userManager.getUserLogins(UserRole.MANAGER);

        System.out.println(UserRole.USER.getFilename());
        System.out.println(UserRole.ADMINISTRATOR.getFilename());
        System.out.println(UserRole.MANAGER.getFilename());

        UserRole[] roles = UserRole.values();
        for (UserRole r : roles) {
            System.out.println(r.name() + " " + r.ordinal());
        }

        UserRole fromString = UserRole.valueOf("user".toUpperCase());
        System.out.println("fromString: " + fromString.name());
        UserRole fromString2 = UserRole.valueOf("user".toUpperCase());

        System.out.println(fromString == UserRole.USER);
        System.out.println(fromString == fromString2);
        System.out.println(fromString.equals(fromString2));

    }

}
