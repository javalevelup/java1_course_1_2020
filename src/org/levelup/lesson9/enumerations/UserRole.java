package org.levelup.lesson9.enumerations;

import java.util.function.Function;

// 1. Может ли enum быть наследником другого класса? Нет
// 2. Может ли enum быть наследником другого enum? Нет
// 3. Может ли enum быть наследником абстрактного класса? Нет
// 4. Может ли enum реализовывать интерфейс? Да
// 5. Может ли enum иметь абстрактные методы? Да
// class UserRole extends Enum {}
public enum UserRole implements Function<String, String> {

    // upper case
    USER("users.txt") {
        @Override
        public String getBaseFilepath() {
            return "/users/";
        }

        @Override
        public String apply(String s) {
            return null;
        }
    },
    MANAGER("managers.txt") {
        @Override
        public String getBaseFilepath() {
            return "/managers/";
        }

        @Override
        public String apply(String s) {
            return null;
        }
    },
    ADMINISTRATOR("administrators.txt") {
        @Override
        public String getBaseFilepath() {
            return "/administrators/";
        }

        @Override
        public String apply(String s) {
            return null;
        }
    };

    private String filename;

    UserRole(String filename) {
        System.out.println("UserRole constructor");
        this.filename = filename;
    }

    public String getFilename() {
        return filename;
    }

//    @Override
//    public String apply(String s) {
//        return null;
//    }

    public abstract String getBaseFilepath();

}
