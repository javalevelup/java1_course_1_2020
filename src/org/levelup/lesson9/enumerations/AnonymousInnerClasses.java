package org.levelup.lesson9.enumerations;

import org.levelup.lesson5.Point;

import java.util.function.Function;
import java.util.function.Predicate;

@SuppressWarnings("ALL")
public class AnonymousInnerClasses {

    // full class name: org.levelup.lesson9.AIC
    // full class name: org.levelup.lesson9.AIC$NonNullPredicate
    // full class name: org.levelup.lesson9.AIC$1
    // full class name: org.levelup.lesson9.AIC$2
    public static void main(String[] args) {
        // integer -> to hex string

        Point point = new Point(3, 4) {
            @Override
            public String printPoint() {
                return "y: " + y + ", x:" + x;
            }
        };

        String prefix = "0x";
        Function<Integer, String> transformIntegerToHexStringFunction = new Function<Integer, String>() {
            @Override
            public String apply(Integer integer) {
                return prefix + Integer.toHexString(integer).toUpperCase();
            }
        };

        String octalPrefix = "0";
        Function<Integer, String> transformIntegerToOctalStringFunction = new Function<Integer, String>() {
            @Override
            public String apply(Integer integer) {
                return octalPrefix + Integer.toString(integer, 8);
            }
        };

        String hexString = transformIntegerToHexStringFunction.apply(43454);
        String octalString = transformIntegerToOctalStringFunction.apply(23);
        System.out.println(hexString);
        System.out.println(octalString);
    }

    class NonNullPredicate implements Predicate<Object> {
        @Override
        public boolean test(Object o) {
            return o != null;
        }
    }

}
