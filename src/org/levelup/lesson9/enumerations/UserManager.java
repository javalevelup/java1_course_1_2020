package org.levelup.lesson9.enumerations;

import java.util.ArrayList;
import java.util.Collection;

public class UserManager {

    // user, admin, manager
    public Collection<String> getUserLogins(String role) {
        // role == user, admin, manager
        return new ArrayList<>();
    }

    public Collection<String> getUserLogins(UserRole role) {
        return new ArrayList<>();
    }

}
