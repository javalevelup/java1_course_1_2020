package org.levelup.lesson1;

@SuppressWarnings("ALL")
public class HelloWorld {

    public static void main(String[] args) {
        System.out.println("Hello world!");

        int a;
        // Сохраняем в переменную а значение 145
        a = 145;

        int b = 5096; // int b; b = 5096;
        int sum = a + b;
        System.out.println("Сумма: " + sum); // "Сумма: " + "5241" -> "Сумма: 5241"

        System.out.println("Сумма: " + (a + b)); // "Сумма: 1455096"
        System.out.println(a + b + ": сумма"); // "5241: сумма"

        // a = 145;
        // a++; a = a + 1; -> a = 146;
        a++; // ++a; - increment


        int var = 20;
        System.out.println(var++); // 20
        System.out.println(++var); // 22
        // var = 22

        // int var = 20;
        // System.out.println(var);
        // var = var + 1;
        // var = var + 1;
        // System.out.println(var);

    }

}
