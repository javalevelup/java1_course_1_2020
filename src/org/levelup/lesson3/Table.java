package org.levelup.lesson3;

// TODO: Table in Table
// [16B, 8B weight, 8B length] - 32B

// String class - 64
// class Table { String name } - 24
// class Table { boolean b } - 24
// class Table { int a; } - 16 + 4 = 24
// class Table { int a; int b} -  16 + 4 + 4 = 24

// void m(int a);
// void m(int b);
// int m(int a);

// void meth(double a, double b) {}
// void meth(int a, double b) {}
// void meth(double b, int a) {}

// void meth(double b, double a) {}


// Access modifiers (модификаторы доступа)
// поле, методы, кострукторы имеют модификатор доступа
//  private - доступ только внутри класса
//  default-package (private-package) - доступ внутри класса и внутри пакета
//  protected
//  public - доступ отовсюду
public class Table {

    double weight = 43.23; // переменная класса, field (поле класса)
    double length; // -> 0.0
    double width;
    double height;
    String name;  // null

    private String color;

    // constructor
    // public Table() {} - не создается
    Table(String name) {
        System.out.println("Constructor Table");
        this.name = name;
        this.color = "Black";
    }

    Table(String name, double weight) {
        this.name = name;
        this.weight = weight;
        this.color = "White";
    }

    public Table() {}

    // calculateVolume

    // void main(String[] args) { sout("Hello"); }
    // return type - тип результата работы метода (void)
    // method name - название метода
    // method arguments (method parameters) - входные параметры
    // method body - тело метода (то что метод делает)

    double calculatePrice() {
        // volume * weight * 0.05
        return calculateVolume() * weight * 0.05;
    }

    // 0 to 99.9
    // calculatePriceWithDiscount(int discount) {}
    double calculatePrice(double discount) {
        return calculateVolume() * weight * 0.05 * ((100 - discount) / 100);
    }

    // называется с МАЛЕНЬКОЙ буквы
    public double calculateVolume() {
        // double volume = length * width * height;
        // return volume;
        return this.length * width * height * 2;
    }

    // double calculatePrice(double var) {}

    // (Type name, Type name2, Type name3)
    // (double dValue, String sValue, int iValue)
    public void changeName(String newName) {
        // "" - true, otherwise false
        if (!newName.isEmpty()) { // if (newName.length() != 0)
            name = newName;
        }
    }

}
