package org.levelup.lesson3;

@SuppressWarnings("ALL")
public class SelectMethod {

    public static void main(String[] args) {
        long a = 1;
        float b = 1f;
        method(a, b);
    }

    static void method(double a, double b) {}

    static void method(int a, double b) {}

    static void method(double a, int b) {}

}
