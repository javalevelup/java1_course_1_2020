package org.levelup.lesson3;

public class App {

    public static void main(String[] args) {
        // int a; - локальная переменная
        // boolean b;

        // double[] weights;
        // double[] lengths;
        // String[] names;

        double[] weights = new double[10];
        double[] lengths = new double[10];

        double[] sofaWeights;
        double[] sofaLengths;

        weights[0] = 15.43;
        lengths[0] = 140;

        int a = 40;

        // object, reference, instance (объект, ссылка, экземпляр)
        Table t = new Table("T table");

        System.out.println("Вес до изменения: " + t.weight);

        t.weight = 15.43;
        t.length = 140;
        t.height = 60;
        t.width = 60;

        System.out.println("Вес стола: " + t.weight);
        System.out.println("Длина стола: " + t.length);

        Table workTable = new Table("Work table", 56.45);

        workTable.length = 160;

        System.out.println("Вес workTable: " + workTable.weight);
        System.out.println("Длина workTable: " + workTable.length);

        double volume = t.length * t.height * t.width;
        System.out.println("Объем стола: " + volume);

        double tVolume = t.calculateVolume();
        System.out.println("Объем стола через метод: " + tVolume);

        double price = t.calculatePrice();
        System.out.println("Цена перевозки: " + price);
//        System.out.println("Цена перевозки: " + t.calculatePrice());

        double discountPrice = t.calculatePrice(10);
        System.out.println("Цена со скидкой: " + discountPrice);


        //
        double workTableVolume = workTable.length * workTable.height * workTable.width * 2;

        workTable.changeName("Desk");
        System.out.println("Название workTable: " + workTable.name);
        workTable.changeName("");
        System.out.println("Название workTable после изменения: " + workTable.name);

        Table table = new Table("Table", 54.23);
        System.out.println(table.name + " " + table.weight);
        // table.color = "";

//        Table undefined = null;
//        undefined.changeName(""); // -> NullPointerException (NPE)

//        Table undefinedName = new Table();
//        System.out.println(undefinedName.length);
//        System.out.println(undefinedName.name.length());


        Table[] tables = new Table[10];

    }

}
