package org.levelup.lesson4;

public class StaticExample {

    public static void main(String[] args) {
        // CurrencyConverter converter = new CurrencyConverter();
        // converter.fromEUR();
        double rub = CurrencyConverter.fromEUR(59);
        System.out.println(rub);

        System.out.println("Курс евро: " + CurrencyConverter.eurExchangeRate);
        CurrencyConverter.eurExchangeRate = 69.76;
        System.out.println("Курс после изменения: " + CurrencyConverter.eurExchangeRate);
    }

}
