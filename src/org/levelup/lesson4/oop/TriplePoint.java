package org.levelup.lesson4.oop;

// extends
// Point - parent class, base class, superclass
// TriplePoint - subclass, подкласс
public class TriplePoint extends Point {

    int z;

    public TriplePoint() {
        super(); // вызов коснструктора суперкласса
        System.out.println("TriplePoint constructor");
    }

    public TriplePoint(int x, int y, int z) {
        super(x, y);
        this.z = z;
    }

    // Переопределение методов - Overriding
    public void displayCoordinate() {
        // super.displayCoordinate(); // вызов родительского метода
        System.out.println("(" + x + "," + y + "," + z + ")");
    }

}
