package org.levelup.lesson4.oop;

public class OopApp {

    public static void main(String[] args) {
        Point point = new Point();
        TriplePoint triplePoint = new TriplePoint();

        point.setX(10);
        triplePoint.setX(14);
        triplePoint.displayCoordinate();

        // Object
        //      Point
        //          TriplePoint


        // Инкапсуляция
        // 1. Скрытие данных от всех
        point.setY(10);
        System.out.println("y: " + point.getY());

        // 2. Данные и методы должны быть в паре
        //      Нужно объединять данные и методы для работы с этими данными вместе в одном классе
        Point p1 = new Point();
        p1.setX(10);
        p1.setY(14);

        Point p2 = new Point();
        p2.setX(14);
        p2.setY(20);

        double dist = calculateDistance(p1, p2); // Procedure
        System.out.println("Distance (p1, p2): " + dist);

        double distFromPoint = p1.calculateDistance(p2); // OOP
        // p1.calculateDistance(p1, p2);
        System.out.println("Distance p1.p2: " + distFromPoint);

        TriplePoint tp = new TriplePoint(39, 65, 34);
        tp.displayCoordinate();


        System.out.println();
        System.out.println();
        // Приведение типов
        TriplePoint triple = new TriplePoint(45,32,44);
        Point fromTriple = triple;
        fromTriple.displayCoordinate();
        Object fromPoint = fromTriple;

        TriplePoint fromObject = (TriplePoint) fromPoint;

        fromObject.displayCoordinate();
        fromTriple.displayCoordinate();

        System.out.println(fromPoint.getClass().getName());

    }

    static double calculateDistance(Point p1, Point p2) {
        double xDistance = p2.getX() - p1.getX();
        double yDistance = p2.getY() - p1.getY();
        // a * a + b * b = c * c
        return Math.sqrt(xDistance * xDistance + yDistance * yDistance);
    }

}
