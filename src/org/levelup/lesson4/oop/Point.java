package org.levelup.lesson4.oop;

public class Point {

    protected int x;
    protected int y = 6;

    public Point() {
        super();
        System.out.println("Point constructor...");
    }

    public Point(int x, int y) {
        System.out.println("Point constructor with x and y...");
        this.x = x;
        this.y = y;
    }

    public void displayCoordinate() {
        System.out.println("(" + x + "," + y + ")");
    }

    // setters: void set<Имя поля>(<Тип поля> <имя поля>) { this.<имя поля> = <имя поля>; }
    // getters: <Тип поля> get<Имя поля>() { return <Имя поля>; }
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    // point.calculateDistance(point2);
    double calculateDistance(Point p) {
        double xDistance = p.x - this.x; // p.x - x
        double yDistance = p.y - this.y; // p.y - y
        // a * a + b * b = c * c
        return Math.sqrt(xDistance * xDistance + yDistance * yDistance);
    }

//    double calculateDistance(Point p1, Point p2) {
//        double xDistance = p2.x - p1.x;
//        double yDistance = p2.getY() - p1.getY();
//        // a * a + b * b = c * c
//        return Math.sqrt(xDistance * xDistance + yDistance * yDistance);
//    }

}
