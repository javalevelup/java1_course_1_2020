package org.levelup.lesson4;

public class CurrencyConverter {

    static double eurExchangeRate = 77.89;
    static double usdExchangeRate = 70.65;

    public static double fromEUR(double eur) {
        // CurrencyConverter converter = new CurrencyConverter();
        // converter.eurExchangeRate
        return eur * eurExchangeRate * usdExchangeRate;
    }

    public void displayExchangeRate() {
        System.out.println(eurExchangeRate);
    }

}
