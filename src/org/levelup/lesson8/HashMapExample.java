package org.levelup.lesson8;

import java.util.*;

public class HashMapExample {

    public static void main(String[] args) {

        HashMap<String, Integer> words = new HashMap<>();

        words.put("help", 34);
        words.put("dog", 122);
        words.put("cat", 454);
        words.put("voice", 12);
        words.put("clear", 54);

        Integer dogWordCount = words.get("dog");
        System.out.println(dogWordCount);

        // display map using keySet()
        System.out.println();
        Set<String> keys = words.keySet();
        for (String key : keys) {
            System.out.println("Key: " + key + ", values: " + words.get(key));
        }

        // display all values
        System.out.println();
        Collection<Integer> values = words.values();
        System.out.println(values);

        // display map using entrySet()
        System.out.println();
        Set<Map.Entry<String, Integer>> entries = words.entrySet();
        // Map.Entry<String,Integer>
        //      getValue(), getKey()
        //      setValue()
        for (Map.Entry<String, Integer> entry : entries) {
            System.out.println("Key: "+ entry.getKey() + ", value: " + entry.getValue());
        }

        // words.forEach((key, value) -> System.out.println("Key: " + key + ", value: " + value));

    }

}
