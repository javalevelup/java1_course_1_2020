package org.levelup.lesson8;

public class OuterClassApp {

    public static void main(String[] args) {
        OuterClass outerClass = new OuterClass();

        // org.levelup.lesson8.OuterClass - full name of class
        // org.levelup.lesson8.OuterClass.InnerClass - full name of inner class

        OuterClass.InnerClass innerClass = outerClass.new InnerClass(76); // создали объект внутреннего класса
        System.out.println(outerClass.getVariable());

        OuterClass outer = new OuterClass();
        outer.new InnerClass(485);

        System.out.println(outer.getVariable());

    }

}
