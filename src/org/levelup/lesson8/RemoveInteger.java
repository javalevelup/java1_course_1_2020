package org.levelup.lesson8;

import java.util.ArrayList;
import java.util.List;

public class RemoveInteger {

    public static void main(String[] args) {
        List<Integer> ints = new ArrayList<>();
        ints.add(1);
        ints.add(2);
        ints.add(3);
        ints.add(4);
        ints.add(5);
        ints.add(6);

        ints.remove(1);
        ints.remove(new Integer(1));

    }

}
