package org.levelup.lesson8.exception;

@SuppressWarnings("ALL")
public class TryCatchFinally {

    public static void main(String[] args) {
//        System.out.println(wtf(10));
//        System.out.println(wtf(11));
        withoutFinally();
    }

    static int wtf(int val) {
        try {
            if (val == 10) {
                return 1;
            }
            throw new RuntimeException();
        } catch (Exception exc) {
            return 2;
        } finally {
            return 3;
        }
    }

    static void withoutFinally() {
        // try-catch-finally
        // try-catch
        // try-finally

        try {
            System.out.println("try");
            System.exit(225);
        } catch (Exception exc) {
            System.out.println("catch");
        } finally {
            System.out.println("finally");
        }

    }

}
