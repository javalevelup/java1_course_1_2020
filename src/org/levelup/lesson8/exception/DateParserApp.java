package org.levelup.lesson8.exception;

import java.text.ParseException;
import java.util.Date;

public class DateParserApp {

    public static void main(String[] args) throws ParseException {
        Date date = new Date(); // now()
        System.out.println(date); //
        System.out.println(date.getTime()); // ms from 1 Jan 1970

        Date parsedDate = new DateParser().parseDate("06g4.04.2020 12:43:43");
        System.out.println(parsedDate);

    }

}
