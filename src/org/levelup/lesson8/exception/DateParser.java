package org.levelup.lesson8.exception;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateParser {

    // string -> Date
    // 03.04.2020 -> date value
    public Date parseDate(String string) {
        // HH: 24 format
        // hh: 12 am/pm
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

        // try-catch-finally
        try {
            return formatter.parse(string);
        } catch (Exception e) {
            System.out.println("Handle exception: " + e.getMessage());
            // e.printStackTrace();
            return null;
            // throw new RuntimeException(e);
        }
    }

}
