package org.levelup.lesson8.exception;

// Unchecked exception
public class ArrayInvalidIndexException extends RuntimeException {

    private int invalidIndex;

    public ArrayInvalidIndexException(int invalidIndex) {
        super("Invalid index in DynamicArray: " + invalidIndex);
        this.invalidIndex = invalidIndex;
    }

    public int getInvalidIndex() {
        return invalidIndex;
    }

}
