package org.levelup.lesson8;

import org.levelup.lesson6.SingleLinkedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@SuppressWarnings("ALL")
public class CollectionExample {

    public static void main(String[] args) {
        ArrayList<Integer> amounts = new ArrayList<>();

        // Добавление элементов в коллекцию
        amounts.add(494);
        amounts.add(4499);
        amounts.add(2134);
        amounts.add(254);
        amounts.add(654);

        List<Integer> otherAmounts = new ArrayList<>();

        otherAmounts.add(495);
        otherAmounts.add(5695);

        amounts.addAll(otherAmounts);

        // Печать коллекции
        System.out.println(amounts.toString());

        // get(index) in ArrayList - O(1)
        // get(index) in LinkedList - O(n)

        for (int i = 0; i < amounts.size(); i++){
            System.out.println(amounts.get(i));
        }

        // foreach
        // for (ClassType variableName : Collection)
        System.out.println();
        for (Integer amount : amounts) {
            System.out.println(amount);
        }

        System.out.println("Using Iterator");
        // Using Iterator
        Iterator<Integer> iterator = amounts.iterator();
        while (iterator.hasNext()) {
            Integer value = iterator.next();
            System.out.println(value);
        }


        System.out.println();
        int[] ints = new int[] { 1, 2, 4, 5, 7, 8 };
        for (int el : ints) {
            System.out.println(el);
        }

        boolean isContains = amounts.contains(2134);
        System.out.println("isContains: " + isContains);

        int index = amounts.indexOf(2134);
        System.out.println("indexOf: " + index);
        System.out.println("indexOf(no element): " + amounts.indexOf(43534));



        SingleLinkedList linkedList = new SingleLinkedList();
        linkedList.add(3404);
        linkedList.add(3474);
        linkedList.add(334);
        linkedList.add(348884);

        Iterator<Integer> linkedListIterator = linkedList.iterator();
        while (linkedListIterator.hasNext()) {
            System.out.println(linkedListIterator.next());
        }

        for (Integer value : linkedList) {
            System.out.println(value);
        }



    }

}
