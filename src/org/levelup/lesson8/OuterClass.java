package org.levelup.lesson8;

public class OuterClass {

    private int variable;

    public void setInnerVar(int innerVar) {
        InnerClass innerClass = new InnerClass(10);
        innerClass.innerVar = innerVar;
    }

    public class InnerClass {

        private int innerVar;

        public InnerClass(int var){
            variable = var;
        }

    }

    public int getVariable() {
        return variable;
    }
}
