package org.levelup.lesson6;

// int
//  - добавить элемент
//  - поиск элементов
//  - удаление элементов
//  - получить размер (количество элементов в структуре)
//  - проверика на пустоту (что нет элементов в структуре)

// Interface:
//     - Все методы абстрактные
//     - Все методы имеют public abstract
//     - Нет понятия конструктора
//     - Нельзя создать объект интерфейса
//          Но можно объявить:
//          Structure s = new DynamicArray();
//          Structure s = new SingleLinkedList();
//          Structure s = new Stack();
//          Structure s = new Queue();
//          Structure s = new Deck();
//     - Все поля в интерфейсе - public static final
public interface Structure {

    // int[][] matrix = new int[10][10]
    // int[][][] 3d = new int[10][10][10]

    void add(int value);

    int getSize();

}
