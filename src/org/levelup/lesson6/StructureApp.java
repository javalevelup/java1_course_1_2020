package org.levelup.lesson6;

public class StructureApp {

    public static void main(String[] args) {
        DynamicArray array = new DynamicArray(4);

        array.add(100);
        array.add(101);
        array.add(102);
        array.add(103);
        array.add(104);
        array.add(105);
        array.add(106);
        array.add(107);

        Structure structure = new DynamicArray(3);
        structure.add(10);
        structure.add(11);
        structure.add(12);
        structure.add(13);
        structure.add(14);
        structure.add(15);
        structure.add(16);

        Structure list = new SingleLinkedList();
        list.add(10);
        list.add(11);
        list.add(12);
        list.add(13);
        list.add(14);
        list.add(15);

//        sort(new Structure() {
//            @Override
//            public void add(int value) {
//
//            }
//
//            @Override
//            public int getSize() {
//                return 0;
//            }
//        });
    }

    // sortList(SingleLinkedList list);
    // sortArray(DynamicArray array);
    static void sort(Structure structure) {
        // ..
        // structure.sort();
        // structure.get(0)
        // structure.set(0, 10);
    }

}
