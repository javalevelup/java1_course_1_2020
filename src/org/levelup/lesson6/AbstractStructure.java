package org.levelup.lesson6;

import org.levelup.lesson8.exception.SingleLinkedListInvalidIndexException;

import java.io.Serializable;

// Абстрактные классы - класс-заготовки
//      Нельзя вызвать конструктор абстратного класса с оператором new (нельзя создать обьект)
//          Нельзя: AbstractStructure as = new AbstractStructure();
//          class A extends AbstractStructure { A() {super();}
//          Можно: AbstractStructure as = new DynamicArray();
//      Могут иметь абстрактные методы
//          Это метод, у которого нет тела
public abstract class AbstractStructure implements Structure, Serializable {

    protected int size;

    @Override
    public int getSize() {
        return size;
    }

    // get(int index) - получение элемента по индексу
    public abstract int get(int index) throws SingleLinkedListInvalidIndexException;

}
