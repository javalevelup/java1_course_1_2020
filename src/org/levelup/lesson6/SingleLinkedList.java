package org.levelup.lesson6;

// Disassemble class - javap -c -v -l SingleLinkedList.class >> SingleLinkedList.txt

import org.levelup.lesson8.exception.SingleLinkedListInvalidIndexException;

import java.util.Iterator;

// объект SingleLinkedList - это и есть связный список
public class SingleLinkedList extends AbstractStructure implements Iterable<Integer> {

    private ListElement head;

    @Override
    public void add(int value) {
        // 40 -> 340 -> 65
        // 675: 40 -> 340 -> 65 -> 675
        ListElement element = new ListElement(value);

        if (head == null) {
            head = element;
        } else {
            ListElement current = head;
            while (current.getNext() != null) {
                current = current.getNext();
            }
            // current - last element
            current.setNext(element); // 65 -> 675
        }

    }

    @Override
    public int get(int index) throws SingleLinkedListInvalidIndexException {
        if (index < 0 || index >= size) {
            throw new SingleLinkedListInvalidIndexException();
        }
        return 0;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new SingleLinkedListIterator();
    }

    // Inner class
    // Nested class - static inner class
    private class SingleLinkedListIterator implements Iterator<Integer> {

        private ListElement current = head;

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public Integer next() {
            Integer value = current.getValue();
            current = current.getNext();
            return value;
        }

    }

}
