package org.levelup.lesson6;

// Список - набор однотипных элементов
// Динамический массив (Список на основе массива) - СД на основе массива, которая позволяет изменять размер

import org.levelup.lesson8.exception.ArrayInvalidIndexException;

public class DynamicArray extends AbstractStructure {

    private int[] elements; // хранятся элементы списка
    // private int size; // количество реальных (добавленных) элементов в дин массиве

    // elements = new int[4];
    // [0, 0, 0, 0], elements.length = 4
    // size = 0!

    // add(4)
    // [4, 0, 0, 0] - size = 1
    // add(8)
    // [4, 8, 0, 0] - size = 2
    // add(0)
    // [4, 8, 0, 0] - size = 3
    // add(10)
    // [4, 8, 0, 10] - size = 4
    // add(14)
    // [4, 8, 0, 10, 14] - size = 5

    public DynamicArray(int initialCapacity) { // первоначальный размер массива
         this.elements = new int[initialCapacity];
         this.size = 0; // это можно не делать :)
    }

    // add - добавляет элемент в конец списка
    @Override
    public void add(int value) {
        if (size == elements.length) { // количество элментов в дин массиве равно размеру массива
            // увеличить размер массива
            int[] oldElements = elements; // сохранить значения из текущего массива elements
            elements = new int[(int)(elements.length * 1.5)]; // создать новый массив большего размера

            // Скопировать oldElements в elements
            System.arraycopy(oldElements, 0, elements, 0, oldElements.length);
        }
        // добавить элемент в массив
        // [0, 0, 0], size =  0
        // add(1) -> [1, 0, 0], size = 1
        // add(5) -> [1, 5, 0], size = 2
        elements[size++] = value;
    }

    // DynamicArray a = new DynamicArray(10);
    // a.add(10); a.add(20);
    // for (;;) a.add(10);

    @Override
    public int get(int index) {
        if (index < 0 || index >= size) {
            throw new ArrayInvalidIndexException(index);
        }
        return elements[index];
    }

    public int[] trim() {
        // size - реальные элементы
        // array.length - все элементы
        return elements;
    }

}
