package org.levelup.lesson6;

public class ListElement {

    private ListElement next;
    // private ListElement prev;
    private int value;

    public ListElement(int value) {
        // this.next = null;
        this.value = value;
    }

    public ListElement getNext() {
        return next;
    }

    public void setNext(ListElement next) {
        this.next = next;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
