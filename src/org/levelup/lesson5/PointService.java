package org.levelup.lesson5;

@SuppressWarnings("ALL")
public class PointService {

    public void print(Point[] points) {
        for (int i = 0; i < points.length; i++) {
            System.out.println(points[i].printPoint());
        }
    }

    public boolean contains(Object[] array, Object object) {
        // return Arrays.stream(array).anyMath(o -> o.equals(object));
        int objectHash = object.hashCode();
        for (int i = 0; i < array.length; i++) {
            if (array[i].hashCode() == objectHash && array[i].equals(object)) {
                return true;
            }
        }
        return false;
    }

}
