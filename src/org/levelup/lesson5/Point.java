package org.levelup.lesson5;

import java.util.Objects;

@SuppressWarnings("ALL")
public class Point {

    protected String name;
    public int x;
    protected int y;

    // Windows: Alt + Insert -> Constructor -> Ok
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public String printPoint() {
        return "(" + x + ", " + y + ")"; // "(x, y)"
    }

    // a == b
    //  true -> a = b or b = a
    //  true -> a = null and b = null

    // p1.equals(p2)
    // p1 - this
    // p2 - object
    @Override
    public boolean equals(Object object) {
        if (this == object) return true;

        // Проверить тип object
        // 1 var
        // instanceof
        // <object name> instanceof <class name>
        // object instanceof Point -> true
        // "" instanceof Point -> false

        // Если object не является типом Point, то тогда результат - false
        // null instanceof Point -> false
        // if (!(object instanceof Point)) return false;

        // 2 var
        // object - TriplePoint, object instanceof Point -> true
        // point.equals(triplePoint) -> true

        // this - Point, object - TriplePoint
        // Point != TriplePoint
        if (object == null || getClass() != object.getClass()) return false;

        // x это this.x и это p1.x
        Point other = (Point) object; // ClassCastException
        // Object o = other;
        // if (this.x == x) // this.x == this.x
        return x == other.x && y == other.y
                // && name.equals(other.name);
                // && (name == other.name || (name != null && name.equals(other.name)));
                && Objects.equals(name, other.name);
    }

    // hashCode
    // хэш-функция, хэш
    // коллизия (коллизия хэш-функции): два разных набора данных дают одинаковый хэш

    // 1. если a.equals(b) == true, то тогда a.hashCode() == b.hashCode()
    // 2. если a.hashCode() == b.hashCode(), то это не означает, что a.equals(b) == true
    @Override
    public int hashCode() {
        // return Objects.hash(x, y, name);
        int result = 31;

        result = 31 * result + x;
        result = 31 * result + y;

        if (name != null) {
            result = 31 * result + name.hashCode();
        }

        return result;
        // return x; если equals вычисляется только по x.
    }

}
