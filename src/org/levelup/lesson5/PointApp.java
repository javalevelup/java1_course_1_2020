package org.levelup.lesson5;

public class PointApp {

    public static void main(String[] args) {
        Point[] allPoints = new Point[5]; // 3 -> 5

        allPoints[0] = new Point(1, 2); // Ctrl + D - copy row
        allPoints[1] = new Point(4, 3);
        allPoints[2] = new Point(5, 7);

        allPoints[3] = new TriplePoint(4, 2, 1);
        allPoints[4] = new TriplePoint(5, 7, 3);

        PointService service = new PointService();

        service.print(allPoints);

        boolean isExist = service.contains(allPoints, new Point(4, 3));
        System.out.println("Is exist: " + isExist);

    }

}
