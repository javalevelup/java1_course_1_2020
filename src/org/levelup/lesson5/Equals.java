package org.levelup.lesson5;

@SuppressWarnings("ALL")
public class Equals {

    public static void main(String[] args) {
        Point p1 = new Point("A", 1, 2);
        Point p2 = new Point("A", 1, 2);

        Point p3 = new Point(null, 1, 2);
        Point p4 = p1;

        // == - сравнение ссылок
        System.out.println(p1 == p2); // -> false

        //System.out.println(p1.equals(null));
        System.out.println(p1.equals(p4));
        //System.out.println(p1.equals(p3));
    }

}
