package org.levelup.lesson5;

public class TriplePoint extends Point  {

    private int z;

    public TriplePoint(int x, int y, int z) {
        super(x, y);
        this.z = z;
    }

    @Override
    public String printPoint() {
        return "(" + x + ", " + y + ", " + z + ")";
    }


}
