package org.levelup.lesson7.generic;

public class GenericListElement<T> {

    private GenericListElement<T> next;
    private T value;

    public GenericListElement(T value) {
        this.value = value;
    }

    public GenericListElement<T> getNext() {
        return next;
    }

    public void setNext(GenericListElement<T> next) {
        this.next = next;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

}
