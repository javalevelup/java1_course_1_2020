package org.levelup.lesson7.generic;

import org.levelup.lesson6.ListElement;

public class GenericSingleLinkedList<T> implements GenericStructure<T> {

    private GenericListElement<T> head;

    @Override
    public void add(T value) {
        GenericListElement<T> element = new GenericListElement<>(value);
        if (head == null) {
            head = element;
        } else {
            GenericListElement<T> current = head;
            while (current.getNext() != null) {
                current = current.getNext();
            }
            current.setNext(element);
        }
    }

}
