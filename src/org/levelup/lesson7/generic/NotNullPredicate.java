package org.levelup.lesson7.generic;

import java.util.function.Predicate;

public class NotNullPredicate<T> implements Predicate<T> {

    @Override
    public boolean test(T t) {
        return t != null;
    }

}
