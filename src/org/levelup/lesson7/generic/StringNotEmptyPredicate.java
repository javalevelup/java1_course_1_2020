package org.levelup.lesson7.generic;

import java.util.function.Predicate;

public class StringNotEmptyPredicate implements Predicate<String> {

    @Override
    public boolean test(String s) {
        return s != null && !s.trim().isEmpty();
    }

}
