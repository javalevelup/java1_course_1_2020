package org.levelup.lesson7.generic;

public class GenericApp {

    public static void main(String[] args) {
        GenericStructure<Object>  structure;
        GenericStructure<Integer> userAccounts = new GenericSingleLinkedList<>();
        GenericStructure<String>  stringStructure = new GenericSingleLinkedList<>();

        stringStructure.add("s1");
        stringStructure.add("s2");
        stringStructure.add("s3");
        stringStructure.add("s4");
        stringStructure.add("s5");
        // stringStructure.add(4765);

        userAccounts.add(5894_0854);
        userAccounts.add(5894_0543);
        userAccounts.add(5894_1234);
        userAccounts.add(1234_1234);
        // userAccounts.add("qwerty");

        GenericStructure<Double> balance = new GenericSingleLinkedList<>();
        balance.add(345.434);
        balance.add(565.454);
        balance.add(23445.454);
        // balance.add("0.0.");

//        structure.add("");
//        structure.add(4);
//
//        stringStructure.add("eqwr");
//        stringStructure.add(4);
//        stringStructure.add(new Object());
//
//        userAccounts.add(54);
//        userAccounts.add(546);
//        userAccounts.add("");
//        userAccounts.add(new Object());

//        structure.add(499);
//        structure.add("String");
//        structure.add(new Object());
    }

}
