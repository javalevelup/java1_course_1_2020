package org.levelup.lesson7.generic;

// Type erasure - стирание типов
public interface GenericStructure<TYPE> {

    void add(TYPE value);

}
