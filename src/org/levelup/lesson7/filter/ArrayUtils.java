package org.levelup.lesson7.filter;

import java.util.Arrays;
import java.util.stream.Collectors;

public class ArrayUtils {

    // filter - EvenNumberFilter
    public int[] filter(int[] array, Filter filter) {

        int[] newArray = new int[array.length];
        int newArrayIndex = 0;

        for (int i = 0; i < array.length; i++) {
            if (filter.test(array[i])) {
                newArray[newArrayIndex++] = array[i];
            }
        }

//        int firstZeroIndex = 0;
//        for (int i = 0; i < array.length; i++) {
//            if (newArray[i] == 0) {
//                firstZeroIndex = i;
//                break;
//            }
//        }
//
//        int[] arr = new int[firstZeroIndex - 1];
//        System.arraycopy(newArray, 0, arr, 0, firstZeroIndex - 1);

        return newArray; // [1, 2, 3, 0, 0, 0];
    }

    public static void main(String[] args) {
        ArrayUtils arrayUtils = new ArrayUtils();
        // EvenNumberFilter filter = new EvenNumberFilter();
        Filter filter = new EvenNumberFilter();
        int[] arr = {1, -2, 4, 5, 6, 7, 8};

        int[] filteredArray = arrayUtils.filter(arr, filter);
        System.out.println(Arrays.toString(filteredArray));

        // Stream API, Lambda functions, functional interfaces, anonymous inner classes
        // int[] newArr = Arrays.stream(arr).filter(element -> element % 2 == 0).toArray();
        // int[] newArr2 = Arrays.stream(arr).filter(element -> element > 0).toArray();
    }

}
