package org.levelup.lesson7.filter;

public class PositiveFilter implements Filter {

    @Override
    public boolean test(int value) {
        return value > 0;
    }

}
