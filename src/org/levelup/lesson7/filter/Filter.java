package org.levelup.lesson7.filter;

public interface Filter {

    // true/false: если value удовлетворяет условию, то тогда возвращаем true
    boolean test(int value);

}
