package org.levelup.lesson7.filter;

public class EvenNumberFilter implements Filter {

    @Override
    public boolean test(int value) {
        return value % 2 == 0; // true, if number is even (если число четное)
    }

}
