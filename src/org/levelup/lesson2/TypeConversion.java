package org.levelup.lesson2;

public class TypeConversion {

    public static void main(String[] args) {
        int intVar = 40;

        long longVar = intVar;
        byte byteVar = (byte) intVar;

        double doubleVar = 45.9;
        int fromDoubleVar = (int) doubleVar;
        System.out.println(fromDoubleVar);
        System.out.println((double)fromDoubleVar);

        byte a = 1;
        byte c = (byte)(a + 1);
        byte ca = ++a;

        long l = 40L;
        float f = 56.34f;
        double d = 56.34;
        double dVar = .508;

        byte b = 10;
        b += 15; // b = (byte) (b + 15);

        ++b;
        boolean result = b + 1 > 100 || b < 50;
    }


}
