package org.levelup.lesson2;

public class ArrayExample {

    public static void main(String[] args) {
        // array
//        double acc1;
//        double acc2;
//        double acc3;
//        double acc4;
//        double acc5;
//        double acc6;
//        double acc7;
//        double acc8;

        // double total = acc1 + acc2 + acc3 + ... + acc7 + acc8;

        //   0    1    2                        7
        // [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        double[] accounts = new double[8];
        accounts[0] = 1045.45;
        accounts[2] = 596504.34;
        System.out.println("Счет первого сотрудника: " + accounts[0]);

        for (int index = 0; index < accounts.length; index++) {
            System.out.println(index + " " + accounts[index]);
        }

    }

}
