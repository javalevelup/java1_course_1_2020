package org.levelup.lesson2;

public class Loops {

    public static void main(String[] args) {
        int scope = 10;
        for (int counter = 1; counter < 10; counter *= 2) {
            // sout
            int loopScope = 10;
            System.out.println("Hell world " + scope * counter);
        }

        int counter = 1;
        for (;counter < 100;) {
            counter *= 3;
        }
        System.out.println(counter);
    }

}
