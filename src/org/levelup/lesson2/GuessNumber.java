package org.levelup.lesson2;

import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

    // psvm
    public static void main(String[] args) {
        // sout

//        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
//            int n = Integer.parseInt(reader.readLine());
//            String line = reader.readLine();
//
//            char[] buf = new char[2048];
//            int readCount = reader.read(buf, 0, 1000); //
//
//        } catch (IOException exc) {
//            throw new RuntimeException(exc);
//        }

        Random r = new Random();
        // nextInt(10) -> [0, 10)
        // nextInt(100) -> [0, 100)
        // nextInt(90) + 10 -> [0 + 10, 90 + 10) -> [10, 100) [10, 99]
        int secretNumber = r.nextInt(6); // [0, 5]

        Scanner sc = new Scanner(System.in);

        int number;
        do {
            System.out.println("Введите число:");
            number = sc.nextInt();
            if (number > secretNumber) {
                System.out.println("Вы ввели больше!");
            } else if (number < secretNumber) {
                System.out.println("Вы ввели меньше");
            }
        } while (number != secretNumber);

        System.out.println("Вы угадали!");


//        int number = sc.nextInt();
//        System.out.println("Вы ввели число: " + number);
//
//        // если number совпал с secretNumber, то мы угадали, иначе не угадали
//        if (number == secretNumber) {
//            System.out.println("Вы угадали!");
//        } else {
//            if (number > secretNumber) {
//                System.out.println("Вы ввели число, которое больше чем загаданное");
//            } else {
//                System.out.println("Вы ввели число, которое меньше чем загаданное");
//            }
//            System.out.println("Вы не угадали число! Секретное число: " + secretNumber);
//        }

//        if..else if..
//        if (number == secretNumber) {
//            System.out.println(1);
//        } else if (number > secretNumber) {
//            System.out.println(2);
//        } else {
//            System.out.println(3);
//        }

    }

}
